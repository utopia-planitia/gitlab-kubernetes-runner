#!/bin/sh

set -o errexit
set -o nounset

# https://kubernetes.io/docs/tasks/run-application/access-api-from-pod/
APISERVER=https://kubernetes.default.svc
SERVICEACCOUNT=/var/run/secrets/kubernetes.io/serviceaccount
NAMESPACE=$(cat "${SERVICEACCOUNT:?}/namespace")
TOKEN=$(cat "${SERVICEACCOUNT:?}/token")
CACERT=${SERVICEACCOUNT:?}/ca.crt

kubectl config set-credentials tests --token="${TOKEN:?}" >/dev/null
kubectl config set-cluster tests --server="${APISERVER:?}" --certificate-authority="${CACERT:?}" >/dev/null
kubectl config set-context tests --cluster=tests --namespace="${NAMESPACE:?}" --user=tests >/dev/null
kubectl config use-context tests >/dev/null

is_the_runner_allowed_to() (
	VERB=${1:?VERB}
	TYPE=${2:?TYPE}
	set -o xtrace
	kubectl auth can-i "${VERB:?}" "${TYPE:?}" --as system:serviceaccount:gitlab-runner:gitlab-runner
)

# minimal permissions required for the interactive web terminal
# https://docs.gitlab.com/ee/ci/interactive_web_terminal/
is_the_runner_allowed_to create pod/exec
is_the_runner_allowed_to get pod/exec
