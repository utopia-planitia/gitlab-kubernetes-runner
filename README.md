# Gitlab Runner for Kubernetes

A deployment of Gitlab Running
https://docs.gitlab.com/runner/install/kubernetes.html

## Setup

to create a config please run `` docker run -ti -v `pwd`/config:/etc/gitlab-runner gitlab/gitlab-runner:alpine-v9.0.0 register ``
