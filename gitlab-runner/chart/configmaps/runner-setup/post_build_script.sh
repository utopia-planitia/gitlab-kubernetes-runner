#!/bin/sh

if [ "${CI_JOB_NAME}" = "${SLEEP_JOB_NAME}" ] && [ "${CI_PIPELINE_ID}" = "${SLEEP_PIPELINE_ID}" ]; then
  echo sleep ${SLEEP_POST_TIME};
  sleep ${SLEEP_POST_TIME};
fi
